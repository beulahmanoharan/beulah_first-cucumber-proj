Feature: Login for LeafTaps Application

Background:
Given Open the Chrome Browser
And Maximize the Browser
And Load the URL
And Wait till the Page Loads

Scenario Outline: TC001 Positive Login Flow
Given Enter the Username as <username>
Given Enter the Password as <password>
When Click on Login Button
Then Verify User navigates to Home Page

Examples:
|username|password|
|DemoSalesManager|crmsfa|
|Demo|crmsfa|


