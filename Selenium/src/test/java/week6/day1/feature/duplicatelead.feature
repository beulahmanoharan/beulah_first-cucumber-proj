Feature: Duplicate Lead for LeafTaps Application

Background:
Given Open the Chrome Browser
And Maximize the Browser
And Load the URL
And Wait till the Page Loads
And Enter the Username
And Enter the Password
When Click on Login Button
Then Verify User navigates to Home Page
When Click on CRMSFA Link
Then Verify User navigates to Leads page
When Click on Leads Link
Then Verify User navigates to My Leads page

Scenario Outline: TC005 Duplicate Lead
When Click on Find Leads option
Then Verify User navigates to Find Leads page
When Click on Email tab
Then User navigates to the Page with EmailId search criteria
Given Enter Email Id as search criteria <EmailId>
When Click on Find Leads button
Then Search results are fetched
Given Wait until the grid loads
When Click on First Resulting Lead Name
Then Verify User navigates to View Lead page
When Click on Duplicate button
Then Verify User navigates to Duplicate Lead page
When Click Create Lead Button
Then Verify User navigates to View Lead page
Given Duplicated Lead Name is fetched
Then Verify Duplicated Lead Name matches

Examples:
|EmailId|
|beulahm87@gmail.com|